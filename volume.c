#include <stdio.h>
#include <pulse/pulseaudio.h>

pa_mainloop *loop;
pa_mainloop_api *api;
pa_context *ctx;
int ret;

void print_info(pa_context *c,
                const pa_sink_info *info,
                int eol,
                void *userdata)
{
  if (eol) {
    return;
  }
  printf("Sink %s\n", info->name);
  printf("Channels %u\n", (unsigned int) info->volume.channels);
  printf("UI MAX %u\n", PA_VOLUME_UI_MAX);
  for (int i = 0; i < info->volume.channels; i++) {
    double vol = info->volume.values[i] / (double) PA_VOLUME_NORM;
    printf("vol %lf\n", vol);
  }
}

void run(void) {

  pa_operation *op;

  op = pa_context_get_sink_info_list(ctx, print_info, NULL);

  if (NULL == op) {
    printf("Failed to create op\n");
    int err = pa_context_errno (ctx);
    printf("Error: %s\n", pa_strerror(err));
    exit(1);
  }

}


void context_state_cb(pa_context *c, void *userdata) {
  static int run_called = 0;
  printf("State changed\n");
  enum pa_context_state state = pa_context_get_state(c);
  if (PA_CONTEXT_READY == state) {
    printf("Ready\n");
    if (!run_called) {
      run();
      run_called = 1;
    }
  }
}

int main() {
  int success;
  loop = pa_mainloop_new();
  if (NULL == loop) {
    printf("Failed to create loop\n");
    exit(1);
  }
  api = pa_mainloop_get_api(loop);
  if (NULL == api) {
    printf("Failed to get api\n");
    exit(1);
  }
  ctx = pa_context_new(api, "volume");
  if (NULL == ctx) {
    printf("Failed to create ctx\n");
    exit(1);
  }

  success = pa_context_connect (ctx, NULL, 0, NULL);
  if (-1 == success) {
    printf("Failed to connect context\n");
    exit(1);
  }

  pa_context_set_state_callback (ctx, context_state_cb, NULL);
  
  printf("Starting main loop\n");
  pa_mainloop_run(loop, &ret);
}
